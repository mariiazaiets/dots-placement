function showDotsPlacement(str) {
    // array of combination [true, false] if true we should show dot otherwise not
    const combinations = getAllPermutationCombinations([true, false], str.length - 1);
    // map combination to word with dots
    const words = combinations
        .map(combination => mapCombinationToWord(combination, str));
    console.log(words.length);
    console.log(words);
}
showDotsPlacement('abc');

function mapCombinationToWord(combination, word) {
    let result = word[0];

    for (let i = 1; i < word.length; i++) {
        if (combination[i - 1]) {
            result = result + '.';
        }
        result = result + word[i];
    }

    return result;
}

function getAllPermutationCombinations(characters, combinationSize) {
    const charactersCount = characters.length;
    const combinationsArray = [];
    getAllPermutationOptionsRecursive(characters, [], charactersCount, combinationSize, combinationsArray);
    return combinationsArray;
}

function getAllPermutationOptionsRecursive(characters, currCombination, charactersCount, combinationSize, combinationsArray) {
    if (combinationSize === 0) {
        combinationsArray.push(currCombination);
        return;
    }
    for (let i = 0; i < charactersCount; i++) {
        const newCurrCombination = [...currCombination, characters[i]];
        getAllPermutationOptionsRecursive(characters, newCurrCombination, charactersCount, combinationSize - 1, combinationsArray);
    }

}
